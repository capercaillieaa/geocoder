package main

import (
	"errors"
	"fmt"
	"net"
	"strings"
)

const answerSize = 2048
const protocolName = "tcp"
const address = "127.0.0.1:4545"

func main() {
	conn, err := getConnection(protocolName, address)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()

	for {
		var sourceData string
		fmt.Print("Введите исходные данные: ")
		_, err := fmt.Scanln(&sourceData)
		if err != nil {
			fmt.Println("Некорректный ввод", err)
			continue
		}

		err = sendSourceData(&conn, sourceData)
		if err != nil {
			fmt.Println(err)
			break
		}

		answer, err := recieveAnswer(&conn)
		if err != nil {
			fmt.Println(err)
			break
		}

		printAnswer(answer)
	}
}

func getConnection(protocol, address string) (net.Conn, error) {
	conn, err := net.Dial(protocol, address)
	if err != nil {
		return nil, errors.New("Не удалось подключиться к серверу")
	}
	return conn, nil
}

func sendSourceData(conn *net.Conn, sourceData string) error {
	n, err := (*conn).Write([]byte(sourceData))
	if n == 0 || err != nil {
		return errors.New("Не удалось отправить исходные данные на сервер")
	}
	return nil
}

func recieveAnswer(conn *net.Conn) (string, error) {
	answer := make([]byte, answerSize)
	n, err := (*conn).Read(answer)
	if err != nil {
		return "", errors.New("Не удалось получить ответ от сервера")
	}
	return string(answer[:n]), nil
}

func printAnswer(answer string) {
	answers := strings.Split(answer, ":")
	fmt.Println("Координаты:", answers[0])
	fmt.Println("Адрес:", answers[1])
}
