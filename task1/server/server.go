package main

import (
	"errors"
	"fmt"
	"net"
	"net/http"

	"github.com/buger/jsonparser"
)

const requestDataSize = 2048
const protocolName = "tcp"
const address = ":4545"

const requestPath = "https://geocode-maps.yandex.ru/1.x/"
const apiKey = "?apikey=d298b311-7248-4048-9a5c-85bf569cf99e"
const requestFormat = "&format=json"
const numOfRequestResults = "&results=1"

const responseDataSize = 2048

func main() {
	listener, err := net.Listen(protocolName, address)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer listener.Close()

	fmt.Println("Сервер слушает...")
	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			conn.Close()
			continue
		}
		go handleConnection(&conn)
	}
}

func handleConnection(conn *net.Conn) {
	defer (*conn).Close()

	for {
		requestData, err := getRequestData(conn)
		if err != nil {
			fmt.Println(err)
			break
		}

		requestBody := buildRequest(requestData)
		response, err := getResponse(requestBody)
		if err != nil {
			fmt.Println(err)
			break
		}
		defer response.Body.Close()

		bytesResp, err := convertRespToBytes(response)
		if err != nil {
			fmt.Println(err)
			break
		}

		answer, err := getAnswer(bytesResp)
		if err != nil {
			fmt.Println(err)
			break
		}

		fmt.Println(requestData, "-", answer)
		sendAnswer(conn, []byte(answer))
		if err != nil {
			fmt.Println(err)
			break
		}
	}
}

func getRequestData(conn *net.Conn) (string, error) {
	requestData := make([]byte, requestDataSize)
	n, err := (*conn).Read(requestData)
	if n == 0 || err != nil {
		return "", errors.New("Серверу не удалось получить данные")
	}
	return string(requestData[:n]), nil
}

func buildRequest(requestData string) (request string) {
	requestData = "&geocode=" + requestData
	request = requestPath + apiKey + requestFormat + numOfRequestResults + requestData
	return
}

func getResponse(requestBody string) (*http.Response, error) {
	resp, err := http.Get(requestBody)
	if err != nil {
		return nil, errors.New("Не удалось отправить запрос к API")
	}
	return resp, nil
}

func convertRespToBytes(response *http.Response) ([]byte, error) {
	bytesBuff := make([]byte, responseDataSize)
	n, err := response.Body.Read(bytesBuff)
	if err != nil || n == 0 {
		return nil, errors.New("Не удалось преобразовать ответ в набор байт")
	}
	return bytesBuff[:n], nil
}

func getAnswer(bytesResp []byte) (string, error) {
	answer := ""
	coords, err := parseJSON(bytesResp, "response", "GeoObjectCollection", "featureMember", "[0]", "GeoObject", "Point", "pos")
	if err != nil {
		return "", err
	}
	address, err := parseJSON(bytesResp, "response", "GeoObjectCollection", "featureMember", "[0]", "GeoObject", "metaDataProperty", "GeocoderMetaData", "text")
	if err != nil {
		return "", err
	}
	answer += (coords + ":" + address)
	return answer, nil
}

func parseJSON(bytesResp []byte, path ...string) (string, error) {
	answer, err := jsonparser.GetString(bytesResp, path...)
	if err != nil {
		return "", errors.New("Не удалось найти заданный путь в ответе")
	}
	return answer, nil
}

func sendAnswer(conn *net.Conn, answer []byte) error {
	_, err := (*conn).Write(answer)
	if err != nil {
		return errors.New("Серверу не удалось отправить ответ для клиента")
	}
	return nil
}
